import random

from edb import cross_validation
from eviar.Fimed import Fimed
import time
from evirs.ECFAR import DecisionRule, ECFAR
from evirs.Recommender import Recommender
import json


tic = time.perf_counter()


with open("datasets/rating_1.json") as file:
    dataset = json.load(file)
    data = cross_validation(dataset, 5, 0)
    n = random.randint(0, 4)
    for i in range(n, n+1):
        ds = data[0]
        print("############################### Iteration "+str(i)+" ##############################")
        # ------------------- ECFAR
        print("# ----------------- ECFAR")
        fimed = Fimed(ds["train_data"], domain=[1, 2, 3, 4, 5], min_support=0.05, min_confidence=0.05)
        rules = fimed.rules
        print(f"Association rules: {len(rules)}")
        classifier = ECFAR(rules, [1, 2, 3, 4, 5], DecisionRule.SOFT)
        toc = time.perf_counter()
        print(f"Classification rules: {len(classifier.rules)}")
        result = classifier.predict(ds["test_data"], ds["test_label"])
        print(f"Unclassified instances: {classifier.unclassified_instances}")
        distance = 0
        euclidian = 0
        N = 0
        for user, predictions in result.items():
            for movie, rating in predictions.items():
                ratings = dataset[user][movie]
                distance += Recommender.jousselme_distance(ratings, rating["bba"])
                euclidian += Recommender.euclidean_distance(classifier.domain, ratings, rating["bba"])
                N += 1
        print("Total instances: ", N)
        print("Distance (DS-PE1): ", distance/N)
        print("Distance (DS-PE2): ", euclidian/N)


print(f"Execution time {toc - tic:0.4f} seconds")
