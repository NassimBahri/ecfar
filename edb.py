"""Methods to generate and manipulate evidential data sets.
"""

# Authors: Nassim Bahri <bahri.nassim@gmail.com>
#          Mohamed Anis Bach Tobji <anis.bach@esen.tn>
# License: MIT


import itertools
import random
import math


def create_subgroups(n, iterable):
    args = [iter(iterable)] * n
    return ([e for e in t if e is not None] for t in itertools.zip_longest(*args))


def cross_validation(data: dict, n_splits: int = 5, random_state: int = None):
    """Cross validation data split (Also called K-Fold and B-Fold)

    Splits the data set into random train and test subsets. It returs an array of ``n_splits`` columns (folds).
    Each column contains the train data and the test data.

    Parameters
    ----------
    data : dict
        Data to split, format: {'150': {'1': {'4': 1}, '2': {'3': 1}, ...}
    n_splits : int, default = 5
        Number of folds
    random_state : int, default = None
        Controls the shuffling applied to the data before applying the split

    Returns
    -------
    list
        List containing train-test split in the following format:\n
        [{\n
        "train_data":[{"1":{"5":1}},...],\n
        "test_data":[{"1":{"5":1}},...],\n
        "test_labels":["2",...]\n
        },...]

    Examples
    --------
    >>> from edb import cross_validation
    >>> import json
    >>> with open("dataset_example.json") as file:
    >>>     dataset = json.load(file)
    >>> train_test_data = cross_validation(dataset, n_splits=5, random_state=10)
    """
    if random_state is None:
        random_state = 0
    keys = list(data.keys())
    for i in range(0, random_state):
        random.shuffle(keys)
    n_lists = math.ceil(len(keys) / n_splits)
    splits = list(create_subgroups(n_lists, keys))
    result = []
    n_splits = len(splits)
    for i in range(0, n_splits):
        train_data_raw = splits.copy()
        del train_data_raw[i]
        train_data = []
        for td in train_data_raw:
            train_data += td
        line = {"train_data": {}, "test_data": {}, "test_label": {}}
        for k in train_data:
            line["train_data"][k] = data[k]
        for k in splits[i]:
            attribute = _select_attribute(data[k])
            if attribute is None:
                continue
            copie = data[k].copy()
            del copie[attribute]
            line["test_data"][k] = copie
            line["test_label"][k] = attribute
        result.append(line)
    return result


def _select_attribute(instance):
    movies = list(instance.keys())
    for i in range(0, 0): #30
        random.shuffle(movies)
    for id in movies:
        ratings = instance[id]
        focal_elements = list(ratings.keys())
        if not (len(focal_elements) == 1 and focal_elements[0] == '1,2,3,4,5'):
            return id
    return None


def split_test_training(data: dict, size: float = 10):
    keys = list(data.keys())
    for i in range(0, 0):
        random.shuffle(keys)
    split_size = math.ceil(len(keys) / size)
    train_set = {}
    test_set = {}
    for i in range(0, split_size):
        user_id = keys[i]
        train_set[user_id] = data[user_id]
        # generate 5 items
        movies = list(data[user_id].keys())
        test_set[user_id] = {}
        for i in range(0, 30):
            random.shuffle(movies)
        numbers = 1
        for id in movies:
            ratings = data[user_id][id]
            focal_elements = list(ratings.keys())
            if not (len(focal_elements) == 1 and focal_elements[0] == '1,2,3,4,5'):
                test_set[user_id][id] = ratings
                data[user_id][id] = {"1,2,3,4,5": 1.0}
                numbers += 1
            if numbers > 5:
                break
    return train_set, test_set
