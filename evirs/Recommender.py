import math
from itertools import chain, combinations
import numpy as np


class Recommender:

    def __init__(self, domain: list) -> None:
        sorted(domain)
        domain = list(map(lambda x: str(x), domain))
        self.domain = domain

    @staticmethod
    def pignistic(focal_element: str, bba: dict) -> float:
        """Computes the pignistic probability of a given focal element

        Parameters
        ----------
        focal_element : str
            A singleton focal element
        bba : dict
            A normalized bba

        Returns
        -------
        float
            Pignistic probability
        """
        focal_element = set(focal_element.split(","))
        pr = 0
        for fe, mass in bba.items():
            pr += len(focal_element & set(fe.split(","))) / len(set(fe.split(","))) * mass
        return pr

    def max_bl_decision(self, bba: dict) -> str:
        """Computes the maximum belief of all focal elements in order to decide the preference label (rating)
        for unrated item (movie).

        Parameters
        ----------
        bba : dict
            A normalized bba

        Returns
        -------
        str
            Decided focal element (singleton or composite). The predicted rating
        """
        pls = {}
        bels = {}
        for item in self.domain:
            bels[item] = 0
            pls[item] = 0
            for fe in bba:
                element = set(fe.split(","))
                if element.issubset(set(item)):
                    bels[item] += bba[fe]
                if element & set(item):
                    pls[item] += bba[fe]
        bels = sorted(bels.items(), key=lambda x: x[1], reverse=True)
        pls = sorted(pls.items(), key=lambda x: x[1], reverse=True)
        max_bel = bels[0]
        found = False
        for max_pl in pls:
            if max_pl[0] == max_bel[0]:
                continue
            if max_pl[1] >= max_bel[1]:
                break
            else:
                found = True
                break
        if found:
            return str(max_bel[0])
        else:
            label = [max_bel[0]]
            for pl in pls:
                if pl[0] == max_bel[0]:
                    continue
                if pl[1] >= max_bel[1]:
                    label.append(pl[0])
                else:
                    break
            return ",".join(sorted(label))

    @staticmethod
    def dempster_combination(bba1: dict, bba2: dict, round_mass: bool = True) -> dict:
        """Dempster’s rule of combination

        Parameters
        ----------
        bba1 : dict
            A normalized bba from source 1 (format: {"1": 0.031, "2": 0.186, "1,3": 0.093, "1,2,3": 0.69})
        bba2 : dict
            A normalized bba from source 2
        round_mass : bool, default=True
            If True use round function when computing the mass of a focal element

        Returns
        -------
        dict
            Resulted bba
        """
        bba = {}
        # Extract focal elements
        focal_elements = set(bba1.keys())
        focal_elements = focal_elements.union(set(bba2.keys()))
        for focal_element in focal_elements:
            quotient = 0
            divisor = 0
            focal_element_items = set(focal_element.split(","))
            for fe1, mass1 in bba1.items():
                items1 = set(fe1.split(","))
                for fe2, mass2 in bba2.items():
                    items2 = set(fe2.split(","))
                    intersection = items1 & items2
                    if len(intersection) == 0:
                        divisor += mass1 * mass2
                    elif focal_element_items == intersection:
                        quotient += mass1 * mass2
            if divisor == 1:
                result = 0
            else:
                result = quotient / (1 - divisor)
            if round_mass:
                bba[focal_element] = round(result, 3)
            else:
                bba[focal_element] = result
        return bba

    @staticmethod
    def dubois_prade_combination(bba1: dict, bba2: dict) -> dict:
        """Dubois & Prade's rule

        Parameters
        ----------
        bba1 : dict
            A normalized bba from source 1 (format: {"1": 0.031, "2": 0.186, "1,3": 0.093, "1,2,3": 0.69})
        bba2 : dict
            A normalized bba from source 2

        Returns
        -------
        dict
            Resulted bba
        """
        focal_elements = set(bba1.keys())
        bba = {}
        for fe in focal_elements:
            mass_fe = 0
            conflict_fe = 0
            elements = set(fe.split(","))
            for item1, mass1 in bba1.items():
                elements1 = set(item1.split(","))
                for item2, mass2 in bba2.items():
                    elements2 = set(item2.split(","))
                    if elements2 & elements1 == elements:
                        mass_fe += mass1 * mass2
                    if len(elements2 & elements1) == 0 and elements2.union(elements1) == elements:
                        conflict_fe += mass1 * mass2
            bba[fe] = mass_fe + conflict_fe
        return bba

    @staticmethod
    def average_combination(bba1: dict, bba2: dict, n: int) -> dict:
        """Averaging rule of combination

        Parameters
        ----------
        bba1 : dict
            A normalized bba from source 1 (format: {"1": 0.031, "2": 0.186, "1,3": 0.093, "1,2,3": 0.69})
        bba2 : dict
            A normalized bba from source 2
        n : int
            Number of fused sources

        Returns
        -------
        dict
            Resulted bba
        """
        focal_elements = set(bba1.keys()).union(set(bba2.keys()))
        bba = {}
        for fe in focal_elements:
            mass = 0
            if fe in bba1:
                mass = bba1[fe] * n
            if fe in bba2:
                mass += bba2[fe]
            bba[fe] = mass / (n + 1)
        return bba

    @staticmethod
    def powerset(iterable: list) -> list:
        """Generate all subsets of a set (powerset)

        Parameters
        ----------
        iterable : list
            Domain

        Returns
        -------
        list
            List of possible subsets
        """
        s = list(iterable)
        return list(chain.from_iterable(combinations(s, r) for r in range(len(s) + 1)))

    @staticmethod
    def jousselme_distance(bba1: dict, bba2: dict) -> float:
        """Computes the distance between two body of evidences (Jousselme distance)

        Parameters
        ----------
        bba1 : dict
            First bba
        bba2 : dict
            Second bba

        Returns
        -------
        float
            Distance
        """
        if bba2 is None:
            keys = bba1.keys()
            bba2 = {}
        else:
            keys = bba1.keys() | bba2.keys()
        keys = sorted(list(keys))
        difference = []
        for item in keys:
            m1 = 0
            m2 = 0
            if item in bba1:
                m1 = bba1[item]
            if item in bba2:
                m2 = bba2[item]
            difference.append(round(m1 - m2, 2))
        D = []
        for i, item1 in enumerate(keys):
            D.append([])
            for j, item2 in enumerate(keys):
                D[i].append(round(len(set(item1.split(",")) & set(item2.split(","))) / len(set(item1.split(",")) | set(item2.split(","))), 2)
)
        distance = np.transpose(difference).dot(D)
        distance = distance.dot(difference)
        distance = round(math.sqrt(0.5 * distance), 2)
        return distance

    @staticmethod
    def euclidean_distance(domain, bba1, bba2):
        if bba2 is None:
            bba2 = {}
        result = 0
        for fe in domain:
            p1 = Recommender.pignistic(fe, bba1)
            p2 = Recommender.pignistic(fe, bba2)
            result += (p1 - p2) ** 2
        return math.sqrt(result) / math.sqrt(2)