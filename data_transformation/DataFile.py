import random
import json
import webbrowser
from data_transformation.Profile import Profile


class DataFile:
    """
    A class that transform hard rating into soft rating
    Reference: T. L. Wickramarathne, K. Premaratne, M. Kubat and D. Jayaweera,
    "CoFiDS: A Belief-Theoretic Approach for Automated Collaborative Filtering,"
    in IEEE Transactions on Knowledge and Data Engineering, vol. 23, no. 2, pp. 175-189, Feb. 2011.

    Attributes
    ----------
    ratings : dict
        Evidential rating matrix (same structure as ratings_movies)
    ratings_movies : dict
        Rating matrix sorted according to the number of ratings given for movies ([movie][user] = rating)
    movies_limit : int, default=0
        Maximum number of movies
    start : int, default=1
        Start index

    Methods
    -------
    __init__()
        Constructor of the class
    prepare(file_path, start=0, movies_limit=0)
        Load and prepare the data set file
    save(file_path)
        Save the generated data set into json file
    display_rating_matrix(source_file_path, html_file_path)
        Load the original data set and save it in HTML file to visualize the rating matrix
    def display_evi_rating_matrix(html_file_path):
        Display the evidential rating matrix in HTML file
    """

    def __init__(self):
        """Constructor of the class
        """
        self.ratings = {}
        self.ratings_movies = {}
        self.movies_limit = 0
        self.start = 0

    def prepare(self, file_path: str, start: int = 1, movies_limit: int = 0) -> None:
        """Load and prepare the data set file

        Parameters
        ----------
        file_path : str
            Path to the data set file
        start : int, default=1
            Start index
        movies_limit : int, default=0
            Maximum number of movies

        Raises
        ------
        FileNotFoundError
            If the input file does not exist

        Examples
        --------
        >>> from data_transformation.DataFile import DataFile
        >>> file = DataFile()
        >>> file.prepare("datasets/ratings.data", 0, 5)
        """
        if movies_limit > 0:
            self.movies_limit = movies_limit
        self.start = start
        try:
            users_profile = {}
            with open(file_path) as file:
                for line in file:
                    data = line.split("\t")
                    user = str(data[0])
                    movie = data[1]
                    rating = int(data[2])
                    # This loop is used to sort the movies according to the obtained ratings
                    if movie not in self.ratings_movies:
                        self.ratings_movies[movie] = {}
                    self.ratings_movies[movie][user] = rating
                    if user not in self.ratings.keys():
                        self.ratings[user] = {}
                    if user not in users_profile:
                        users_profile[user] = random.randint(1, 3)
                    user_profile = users_profile[user]
                    rating = Profile.transform(rating)
                    self.ratings[user][movie] = rating
                self.ratings_movies = sorted(self.ratings_movies.items(), key=lambda rate: len(rate[1]), reverse=True)
        except FileNotFoundError:
            print(f"Error: The file '{file_path}' does not exist")

    def save(self, file_path: str) -> None:
        """Save the generated data set into json file

        Parameters
        ----------
        file_path : str
            Path to the data set file

        Examples
        --------
        >>> from data_transformation.DataFile import DataFile
        >>> file = DataFile()
        >>> file.prepare("datasets/ratings.data", 0, 5)
        >>> file.save("datasets/out.json")
        """
        if self.movies_limit == 0:
            self.movies_limit = len(self.ratings_movies)
        json_content = {}
        user_number = 1
        for user in self.ratings:
            json_content[user] = {}
            movies_number = 1
            for movie in self.ratings_movies:
                if movies_number < self.start:
                    movies_number += 1
                    continue
                if movies_number == self.movies_limit + self.start:
                    break
                movie_name = movie[0]
                json_content[user][movie_name] = {}
                if movie_name in self.ratings[user]:
                    if isinstance(self.ratings[user][movie_name], tuple):
                        rating = self.ratings[user][movie_name]
                        json_content[user][movie_name][rating[0]] = rating[1]
                    else:
                        for index, rating in enumerate(self.ratings[user][movie_name]["rating_values"]):
                            if isinstance(rating, int):
                                rating_value = rating
                            else:
                                rating_value = ",".join(str(i) for i in rating)
                            json_content[user][movie_name][rating_value] = self.ratings[user][movie_name]["masses"][index]
                else:
                    json_content[user][movie_name]["1,2,3,4,5"] = 1
                movies_number += 1
        with open(file_path, 'w') as file:
            json.dump(json_content, file)

    @staticmethod
    def display_rating_matrix(source_file_path: str, html_file_path: str, open_browser: bool = False) -> None:
        """Load the original data set and save it in HTML file to visualize the rating matrix

        Parameters
        ----------
        source_file_path : str
            Path to the data set file
        html_file_path : str
            Path to the store the output file
        open_browser : boo, Default=False
            If True, open the output file in the web browser

        Raises
        ------
        FileNotFoundError
            If the input file does not exist

        Examples
        --------
        >>> from data_transformation.DataFile import DataFile
        >>> file = DataFile()
        >>> file.display_rating_matrix("datasets/ratings.data", "datasets/out.html", True)
        """
        try:
            ratings_movies = {}
            ratings_users = {}
            with open(source_file_path) as file:
                for line in file:
                    data = line.split("\t")
                    user = data[0]
                    movie = data[1]
                    rating = int(data[2])
                    if movie not in ratings_movies:
                        ratings_movies[movie] = {}
                    if user not in ratings_users:
                        ratings_users[user] = {}
                    ratings_movies[movie][user] = rating
                    ratings_users[user][movie] = rating
                ratings_movies = sorted(ratings_movies.items(), key=lambda rating: len(rating[1]), reverse=True)
                # generate HTML file
                html = open(html_file_path, "w")
                content = """<html><head><style>table{width:100%;border:solid 1px;border-bottom:none;border-right:none} 
                table td, table th{border-right:solid 1px #000;border-bottom:solid 1px #000;
                text-align:center;vertical-align:middle;} </style></head><body><table cellpadding="0" cellspacing="0"><tr>
                <th>#####</th>"""
                index = 1
                for movie in ratings_movies:
                    content += "<th><span style='color:red'>[" + str(index) + "]</span>Movie " + movie[0] + \
                               "<span style='color:red'>[" + str(len(movie[1])) + "]</th>"
                    index += 1
                content += "</tr>"
                index = 1
                for user in ratings_users:
                    content += "<tr><td><span style='color:red'>[" + str(index) + "]</span>User " + user + "</td>"
                    index += 1
                    for movie in ratings_movies:
                        if user in movie[1]:
                            content += "<td>" + str(movie[1][user]) + "</td>"
                        else:
                            content += "<td>-</td>"
                    content += "</tr>"
                content += "</table></body></html>"
                html.write(content)
                html.close()
                if open_browser:
                    webbrowser.open_new_tab(html_file_path)
        except FileNotFoundError:
            print(f"Error: The file '{source_file_path}' does not exist")

    def display_evi_rating_matrix(self, html_file_path: str, open_browser: bool = False) -> None:
        """Display the evidential rating matrix in HTML file

        Parameters
        ----------
        html_file_path : str
            Path to the store the output file
        open_browser : boo, Default=False
            If True, open the output file in the web browser

        Examples
        --------
        >>> from data_transformation.DataFile import DataFile
        >>> file = DataFile()
        >>> file.prepare("datasets/ratings.data", 0, 5)
        >>> file.display_evi_rating_matrix("datasets/evis.html", True)
        """
        if self.movies_limit == 0:
            self.movies_limit = len(self.ratings_movies)
        html = open(html_file_path, "w")
        content = """<html><head><style>table{width:100%;border:solid 1px;border-bottom:none;border-right:none} 
                        table td, table th{border-right:solid 1px #000;border-bottom:solid 1px #000;
                        text-align:center;vertical-align:middle;} </style></head><body><table cellpadding="0" cellspacing="0"><tr>
                        <th>#####</th>"""
        index = 1
        for movie in self.ratings_movies:
            if index < self.start:
                index += 1
                continue
            if index == self.movies_limit + self.start:
                break
            content += "<th><span style='color:red'>[" + str(index) + "]</span>Movie " + movie[0] + \
                       "<span style='color:red'>[" + str(len(movie[1])) + "]</th>"
            index += 1
        content += "</tr>"
        index = 1
        for user in self.ratings:
            content += "<tr><td><span style='color:red'>[" + str(index) + "]</span>User " + user + "</td>"
            index += 1
            movies_number = 1
            for movie in self.ratings_movies:
                if movies_number < self.start:
                    movies_number += 1
                    continue
                if movies_number == self.movies_limit + self.start:
                    break
                movie_name = movie[0]
                if movie_name in self.ratings[user]:
                    if isinstance(self.ratings[user][movie_name], tuple):
                        rating = self.ratings[user][movie_name]
                        content += "<td>m({" + str(rating[0]) + "}) = " + str(rating[1]) + "</td>"
                    else:
                        content += "<td>"
                        for position, rating in enumerate(self.ratings[user][movie_name]["rating_values"]):
                            if isinstance(rating, int):
                                rating_value = rating
                            else:
                                rating_value = ",".join(str(i) for i in rating)
                            content += "m({" + str(rating) + "}) = " + str(self.ratings[user][movie_name]["masses"][position]) + "<br>"
                        content += "</td>"
                else:
                    content += "<td style='color:#dd630f'>m({0}) = 0</td>"
                movies_number += 1
            content += "</tr>"
        content += "</table></body></html>"
        html.write(content)
        html.close()
        if open_browser:
            webbrowser.open_new_tab(html_file_path)

    def save_as_json(self, file_path: str, output_path: str) -> None:
        """Save the original data set in JSON format

        Parameters
        ----------
        file_path : str
            Path to the data set file
        output_path : str
            Path to store the resulted data set

        Raises
        ------
        FileNotFoundError
            If the input file does not exist

        Examples
        --------
        >>> from data_transformation.DataFile import DataFile
        >>> file = DataFile()
        >>> file.save_as_json("datasets/original.json")
        """
        ratings = {}
        try:
            with open(file_path) as file:
                for line in file:
                    data = line.split("\t")
                    user = str(data[0])
                    movie = data[1]
                    rating = int(data[2])
                    if user not in ratings.keys():
                        ratings[user] = {}
                    ratings[user][movie] = rating
            with open(output_path, 'w') as f:
                f.write(repr(ratings))
        except FileNotFoundError:
            print(f"Error: The file '{file_path}' does not exist")
