import webbrowser


class RidLists:
    """
    RidLists Data Structure.


    The RidLists class is a vertical representation of the evidential database
    Reference: Bach Tobji, MA, B. Ben Yaghlane, and Khaled Mellouli.
    "A new algorithm for mining frequent itemsets from evidential databases."
    Proceedings of IPMU. Vol. 8. 2008.

    Attributes
    ----------
    rid : dict
        Rid Lists
    db_size : int
        Size of the database (number of instances)
    domain : list
        Domain of the attributes

    Methods
    -------
    __init__(records, domain)
        Constructor of the class
    bin_code(element)
        Converts focal element to binary code
    generate(records)
        Creates the RidLists structure
    update()
        Updates the RidLists structure
    display(html_file_path)
        Display the Rid Lists in HTML file
    """

    def __init__(self, records: dict, domain: list) -> None:
        """Constructor of the class

        Parameters
        ----------
        records : dict
            A records iterable object (json format)
        domain : list
            Domain of the attributes (in this case all attributes share the same domain)

        Examples
        --------
        >>> from eviar.RidLists import RidLists
        >>> import json
        >>> with open("example_file.json") as file:
        >>>     data = json.load(file)
        >>>     rid = RidLists(data, [1, 2, 3, 4, 5])
        """
        self.rid = {}
        self.db_size = 0
        self.domain = domain
        self.generate(records)
        self.update()

    def bin_code(self, element: str) -> int:
        """Converts focal element to binary code

        Parameters
        ----------
        element : str
            Focal element

        Returns
        -------
        int
            Integer value (representation) of the focal element
        """
        elements = sorted(element.split(","))
        binary = ""
        for item in self.domain:
            if str(item) in elements:
                binary += "1"
            else:
                binary += "0"
        return int(binary, 2)

    def generate(self, records: dict) -> None:
        """Creates the RidLists structure

        Parameters
        ----------
        records : list
            A records iterable object (json format)
        """
        rowid = 1
        for id, line in records.items():
            for attribute, focal_elements in line.items():
                for fe, mass in focal_elements.items():
                    if mass == 0:
                        continue
                    value = attribute + "#" + fe
                    if value not in self.rid:
                        self.rid[value] = {"binary": self.bin_code(fe), "rids": []}
                    self.rid[value]["rids"].append(Item(rowid, mass, mass))
            rowid += 1
        self.db_size = rowid - 1

    def update(self) -> None:
        """Updates the RidLists structure. It updates the belief of the stored focal elements
        """
        keys = self.rid.keys()
        keys = sorted(keys, key=lambda element: len(element.split(",")), reverse=True)
        for key in keys:
            items = self.rid[key]["rids"]
            binary_code = self.rid[key]["binary"]
            values = key.split("#")
            ratings = values[1].split(",")
            if len(ratings) == 1:
                continue
            for key2, items2 in self.rid.items():
                if key2.startswith(values[0]+"#"):
                    if len(key) <= len(key2):
                        continue
                    is_subset = (items2["binary"] & binary_code == items2["binary"])
                    if is_subset:
                        for item in items2["rids"]:
                            exists = False
                            i = 0
                            length = len(items)
                            while i < length and not exists:
                                if item.rowid == items[i].rowid:
                                    exists = True
                                    items[i].bel += item.mass
                                i += 1
                            if not exists:
                                items.append(Item(item.rowid, item.mass, item.bel))

    def display(self, html_file_path: str, open_browser=False) -> None:
        """Display the Rid Lists in HTML file

        Parameters
        ----------
        html_file_path : str
            Path to the store the output file
        open_browser : bool, default=False
            If True, open the browser
        """
        html = open(html_file_path, "w")
        content = """<html><head><style>table{width:100%;border:solid 1px;border-bottom:none;border-right:none} 
                        table td, table th{border-right:solid 1px #000;border-bottom:solid 1px #000;
                        text-align:center;vertical-align:middle;} </style></head><body><table cellpadding="0" cellspacing="0"><tr>
                        <th>Item</th><th>Binary code</th><th>Rid list</th>"""
        keys = self.rid.keys()
        keys = sorted(keys, key=lambda element: len(element.split(",")), reverse=True)
        for focal_element in keys:
            items = self.rid[focal_element]
            content += "<tr><th>" + focal_element + "</th><th>" + str(items["binary"]) + "</th><td>"
            for item in items["rids"]:
                content += "(" + str(item.rowid) + ", " + str(item.bel) + ") "
            content += "</td></tr>"
        content += "</table></body></html>"
        html.write(content)
        html.close()
        if open_browser:
            webbrowser.open_new_tab(html_file_path)


class Item:
    """
    The Item class stores for each focal element its row identifier, its mass and
    its belief value.

    Methods
    -------
    __init__(rowid, mass, bel)
        Constructor of the class
    """

    def __init__(self, rowid, mass, bel):
        """Constructor of the class

        Parameters
        ----------
        rowid : int
            Row identifier of the focal element
        mass : float
            Mass of the focal element
        mass : float
            Belief of the focal element
        """
        self.rowid = rowid
        self.mass = mass
        self.bel = bel
