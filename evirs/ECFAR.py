from evirs.Recommender import Recommender
from enum import Enum


class DecisionRule(Enum):
    HARD = "hard rating"
    SOFT = "soft rating"


class ECFAR(Recommender):
    """
    Evidential Collaborative Filtering based on Association Rules.
    """

    position = 1

    def __init__(self, rules: list, domain: list, decision_rule: DecisionRule) -> None:
        self.rules = []
        self.unclassified_instances = 0
        super().__init__(domain)
        for rule in rules:
            rule_consequent = rule["consequent"]
            focal_element = rule_consequent.split("#")[1]
            if focal_element != "1,2,3,4,5":
                self.rules.append(rule)
        #for r in self.rules:
        #    print(f"rule: {r}")
        self.powerset_elements = super().powerset(self.domain)
        self.decision_rule = decision_rule

    def predict(self, instances: dict, labels: dict):
        if len(self.rules) == 0:
            print("No rules found")
            return []
        classes = {}
        for user, instance in instances.items():
            classes[user] = {}
            k = labels[user]
            classes[user][k] = self.classify(instance, str(k))
        return classes

    def classify(self, instance: dict, attribute: str) -> dict:
        rules = []
        for rule in self.rules:
            if str(rule["consequent"].split("#")[0]) == attribute and str(rule["antecedent"].split("#")[1]) != "1,2,3,4,5":
                rules.append(rule)
        # print("The number of rules is : {}".format(len(rules)))
        instance_attributes = {}
        instance_data = {}
        instance_masses = {}
        instance_focal_elements = []
        # original_class_value = instance[str(attribute)]
        # instance.pop(attribute)
        for attr in instance:
            instance_attributes[attr] = []
            instance_data[attr] = []
            instance_masses[attr] = []
            for focal_element in instance[attr]:
                instance_attributes[attr].append(attr + "#" + focal_element)
                instance_data[attr].append({"focal_element": focal_element, "mass": instance[attr][focal_element]})
                instance_masses[attr].append(instance[attr][focal_element])
                instance_focal_elements.append(attr + "#" + focal_element)
        i = 0
        while i < len(rules):
            rule_parts = rules[i]["antecedent"].split(";")
            rule_mass = 1
            total_intersections = 0
            for rule_part in rule_parts:
                attribute_mass = 0
                elements = rule_part.split("#")
                rule_focal_elements = set(elements[1].split(","))
                instance_values = []
                if elements[0] in instance_data:
                    instance_values = instance_data[elements[0]]
                for fe in instance_values:
                    values = set(fe["focal_element"].split(","))
                    intersection = len(values & rule_focal_elements)
                    union = len(values | rule_focal_elements)
                    if intersection > 0:
                        attribute_mass += fe["mass"] * intersection / union
                if attribute_mass > 0:
                    total_intersections += 1
                rule_mass *= attribute_mass
            if total_intersections == len(rule_parts):
                rules[i]["reliability"] = rule_mass
                i += 1
            else:
                del rules[i]
        if len(rules) == 0:
            self.unclassified_instances += 1
            values = {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0}
            total = 0
            for movie, ratings in instance.items():
                keys = list(ratings.keys())
                if keys[0] != "1,2,3,4,5":
                    values[keys[0]] += 1
                    total += 1
            if total == 0:
                bba = {"1,2,3,4,5": 1}
            else:
                _mass = {}
                for k, v in values.items():
                    _mass[k] = round((v / total) * (1 - 1 / total), 2)
                _mass["1,2,3,4,5"] = round(1 / total, 2)
                bba = {}
                for k, v in _mass.items():
                    if v > 0:
                        bba[k] = v
            return {"rating": "0", "bba": bba}
        bbas = []
        for rule in rules:
            contents = rule["consequent"].split("#")
            rule_class = contents[1].split(",")
            domain = self.domain.copy()
            domain = list(set(domain) - set(rule_class))
            domain = sorted(domain)
            other = ",".join(domain)
            rule_reliability = rule["confidence"] * rule["reliability"]
            other_reliability = (1 - rule["confidence"]) * rule["reliability"]
            bbas.append({
                ",".join(rule_class): round(rule_reliability, 2),
                other: round(other_reliability, 2),
                ",".join(self.domain): round(1 - rule_reliability - other_reliability, 2)
            })
        final_bba = bbas[0]
        for elem in self.powerset_elements:
            elem = sorted(list(elem))
            if len(elem) > 0 and ",".join(elem) not in final_bba:
                final_bba[",".join(elem)] = 0
        lenght = len(bbas)
        n = 1
        for i in range(1, lenght):
            final_bba = ECFAR.dubois_prade_combination(final_bba, bbas[i])
            n += 1
        values = {}
        if self.decision_rule == DecisionRule.HARD:
            for element in self.domain:
                values[element] = round(super().pignistic(str(element), final_bba), 3)
            values = sorted(values.items(), key=lambda item: item[1], reverse=True)
            print(values)
            return {"rating": values[0][0], "bba": final_bba}
        else:
            focal_element = super().max_bl_decision(final_bba)
            result = {}
            for item, value in final_bba.items():
                if value > 0:
                    result[item] = value
            return {"rating": focal_element, "bba": result}
