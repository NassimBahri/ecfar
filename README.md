# ECFAR: A Rule-based Collaborative Filtering System Dealing With Evidential Data


## Usage

```python
from edb import cross_validation
from eviar.Fimed import Fimed
from evirs.ECFAR import DecisionRule, ECFAR
from evirs.Recommender import Recommender
import json


distance = 0
euclidian = 0
with open("datasets/rating_1.json") as file:
    dataset = json.load(file)
    data = cross_validation(dataset, 5, 0)
    ds = data[0]
    fimed = Fimed(ds["train_data"], domain=[1, 2, 3, 4, 5], min_support=0.05, min_confidence=0.05)
    classifier = ECFAR(fimed.rules, [1, 2, 3, 4, 5], DecisionRule.SOFT)
    result = classifier.predict(ds["test_data"], ds["test_label"])
    N = 0
    for user, predictions in result.items():
        for movie, rating in predictions.items():
            ratings = dataset[user][movie]
            distance += Recommender.jousselme_distance(ratings, rating["bba"])
            euclidian += Recommender.euclidean_distance(classifier.domain, ratings, rating["bba"])
            N += 1
    print("Total instances: ", N)
    print("Distance (DS-PE1): ", distance / N)
    print("Distance (DS-PE2): ", euclidian / N)
```


## Authors
* [Nassim Bahri](https://www.nassimbahri.ovh) - *PhD Student in Business Computing*
* [Mohamed Anis Bach Tobji](https://www.esen.tn/portail/enseignant/bachtobji) - *Associate Professor in Business Computing*
* [Boutheina Ben Yaghlane](http://www.larodec.com/member/conseil_scientifique/ben-yaghlane-boutheina) - *Professor in Business Computing*


## License
This project is licensed under the [MIT](https://choosealicense.com/licenses/mit/) License
